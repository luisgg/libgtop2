# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: Tajik Gnome\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=libgtop&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-12-29 00:29+0000\n"
"PO-Revision-Date: 2013-01-21 15:11+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: \n"
"Language: Tajik\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.4\n"

#: ../lib/read.c:51
#, c-format
msgid "read %d byte"
msgid_plural "read %d bytes"
msgstr[0] ""
msgstr[1] ""

#: ../lib/read_data.c:51
msgid "read data size"
msgstr ""

#: ../lib/read_data.c:70
#, c-format
msgid "read %lu byte of data"
msgid_plural "read %lu bytes of data"
msgstr[0] ""
msgstr[1] ""

#: ../lib/write.c:51
#, c-format
msgid "wrote %d byte"
msgid_plural "wrote %d bytes"
msgstr[0] ""
msgstr[1] ""

#: ../src/daemon/gnuserv.c:455
msgid "Enable debugging"
msgstr ""

#: ../src/daemon/gnuserv.c:457
msgid "Enable verbose output"
msgstr ""

#: ../src/daemon/gnuserv.c:459
msgid "Don't fork into background"
msgstr ""

#: ../src/daemon/gnuserv.c:461
msgid "Invoked from inetd"
msgstr ""

#: ../src/daemon/gnuserv.c:495
#, c-format
msgid "Run '%s --help' to see a full list of available command line options.\n"
msgstr ""

#: ../sysdeps/osf1/siglist.c:27 ../sysdeps/sun4/siglist.c:27
msgid "Hangup"
msgstr ""

#: ../sysdeps/osf1/siglist.c:28 ../sysdeps/sun4/siglist.c:28
msgid "Interrupt"
msgstr ""

#: ../sysdeps/osf1/siglist.c:29 ../sysdeps/sun4/siglist.c:29
msgid "Quit"
msgstr "Баромад"

#: ../sysdeps/osf1/siglist.c:30 ../sysdeps/sun4/siglist.c:30
msgid "Illegal instruction"
msgstr ""

#: ../sysdeps/osf1/siglist.c:31 ../sysdeps/sun4/siglist.c:31
msgid "Trace trap"
msgstr ""

#: ../sysdeps/osf1/siglist.c:32 ../sysdeps/sun4/siglist.c:32
msgid "Abort"
msgstr "Қатъ кардан"

#: ../sysdeps/osf1/siglist.c:33 ../sysdeps/sun4/siglist.c:33
msgid "EMT error"
msgstr ""

#: ../sysdeps/osf1/siglist.c:34 ../sysdeps/sun4/siglist.c:34
msgid "Floating-point exception"
msgstr ""

#: ../sysdeps/osf1/siglist.c:35 ../sysdeps/sun4/siglist.c:35
msgid "Kill"
msgstr ""

#: ../sysdeps/osf1/siglist.c:36 ../sysdeps/sun4/siglist.c:36
msgid "Bus error"
msgstr ""

#: ../sysdeps/osf1/siglist.c:37 ../sysdeps/sun4/siglist.c:37
msgid "Segmentation violation"
msgstr ""

#: ../sysdeps/osf1/siglist.c:38 ../sysdeps/sun4/siglist.c:38
msgid "Bad argument to system call"
msgstr ""

#: ../sysdeps/osf1/siglist.c:39 ../sysdeps/sun4/siglist.c:39
msgid "Broken pipe"
msgstr ""

#: ../sysdeps/osf1/siglist.c:40 ../sysdeps/sun4/siglist.c:40
msgid "Alarm clock"
msgstr "Соати зангдор\t"

#: ../sysdeps/osf1/siglist.c:41 ../sysdeps/sun4/siglist.c:41
msgid "Termination"
msgstr "Анҷоми кор"

#: ../sysdeps/osf1/siglist.c:42 ../sysdeps/sun4/siglist.c:42
msgid "Urgent condition on socket"
msgstr ""

#: ../sysdeps/osf1/siglist.c:43 ../sysdeps/sun4/siglist.c:43
msgid "Stop"
msgstr "Истодан"

#: ../sysdeps/osf1/siglist.c:44 ../sysdeps/sun4/siglist.c:44
msgid "Keyboard stop"
msgstr ""

#: ../sysdeps/osf1/siglist.c:45 ../sysdeps/sun4/siglist.c:45
msgid "Continue"
msgstr "Идома додан"

#: ../sysdeps/osf1/siglist.c:46 ../sysdeps/sun4/siglist.c:46
msgid "Child status has changed"
msgstr ""

#: ../sysdeps/osf1/siglist.c:47 ../sysdeps/sun4/siglist.c:47
msgid "Background read from tty"
msgstr ""

#: ../sysdeps/osf1/siglist.c:48 ../sysdeps/sun4/siglist.c:48
msgid "Background write to tty"
msgstr ""

#: ../sysdeps/osf1/siglist.c:49 ../sysdeps/sun4/siglist.c:49
msgid "I/O now possible"
msgstr ""

#: ../sysdeps/osf1/siglist.c:50 ../sysdeps/sun4/siglist.c:50
msgid "CPU limit exceeded"
msgstr ""

#: ../sysdeps/osf1/siglist.c:51 ../sysdeps/sun4/siglist.c:51
msgid "File size limit exceeded"
msgstr ""

#: ../sysdeps/osf1/siglist.c:52 ../sysdeps/sun4/siglist.c:52
msgid "Virtual alarm clock"
msgstr ""

#: ../sysdeps/osf1/siglist.c:53 ../sysdeps/sun4/siglist.c:53
msgid "Profiling alarm clock"
msgstr ""

#: ../sysdeps/osf1/siglist.c:54 ../sysdeps/sun4/siglist.c:54
msgid "Window size change"
msgstr ""

#: ../sysdeps/osf1/siglist.c:55 ../sysdeps/sun4/siglist.c:55
msgid "Information request"
msgstr ""

#: ../sysdeps/osf1/siglist.c:56 ../sysdeps/sun4/siglist.c:56
msgid "User defined signal 1"
msgstr ""

#: ../sysdeps/osf1/siglist.c:57 ../sysdeps/sun4/siglist.c:57
msgid "User defined signal 2"
msgstr ""
